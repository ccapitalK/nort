--[[
--  module containing 
--]]

local state = {}

state.state = {}
state.preHook  = {}
state.postHook = {}

function state.load()
    state.state='mainMenu'
end

--[[
--  State Machine transition function:
--  Runs post and pre-hooks based on corresponding tables
--]]

function state.transition(stateToBe)
    if state.postHook[state] then
        state.postHook[state](stateToBe)
    end
    state.state = stateToBe
    if state.preHook[stateToBe] then
        state.preHook[stateToBe](state)
    end
end

return state
