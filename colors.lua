--[[
--  Module containing color constants
--]]

local colors = {}
colors.arr={}

colors.red    = {0xff, 0x2b, 0x2b, 0xff}
colors.orange = {0xff, 0x9e, 0x21, 0xff}
colors.yellow = {0xff, 0xff, 0x2b, 0xff}
colors.green  = {0x52, 0xff, 0x2b, 0xff}
colors.cyan   = {0x2b, 0xff, 0xd4, 0xff}
colors.blue   = {0x32, 0x71, 0xff, 0xff}
colors.purple = {0x8e, 0x2b, 0xff, 0xff}
colors.pink   = {0xf7, 0x2b, 0xff, 0xff}
colors.white  = {0xff, 0xff, 0xff, 0xff}
colors.black  = {0x00, 0x00, 0x00, 0xff}

colors.arr[1] = colors.red   
colors.arr[2] = colors.orange
colors.arr[3] = colors.yellow
colors.arr[4] = colors.green 
colors.arr[5] = colors.cyan  
colors.arr[6] = colors.blue  
colors.arr[7] = colors.purple
colors.arr[8] = colors.pink  
colors.arr[9] = colors.white 
colors.arr[10]= colors.black 

colors.background = colors.red
colors.foreground = colors.white

return colors
