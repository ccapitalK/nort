--[[
--  Popup Message gameState
--]]

require('utility')
colors=require('colors')


local MSGBOX = {}

function MSGBOX:new(title, width, height, callbacks)
    newObj = {
        title=title,
        width=width,
        height=height,
        callbacks=callbacks or {}
    }
    if newObj.callbacks.setup then
        newObj.callbacks.setup(newObj)
    end
    self.__index = self
    return setmetatable(newObj, self)
end

function MSGBOX:render()
    local font = fonts.optionsFont
    love.graphics.setFont(font)
    local w = self.width 
    local h = self.height 

    local boxW = w - 30
    local nLines = countWrapLines(self.title, font, boxW)
    local boxH = font:getHeight()*nLines
    love.graphics.setColor(colors.foreground)
    love.graphics.rectangle("fill", 300-(w/2), 300-(h/2), w, h)
    love.graphics.setColor(colors.background)
    love.graphics.rectangle("fill", 305-(w/2), 305-(h/2), w-10, h-10)
    love.graphics.setColor(colors.foreground)
    love.graphics.printf(self.title, 300-(boxW/2), 300-(boxH/2), boxW, "center")
end

function MSGBOX:mousepressed(x, y, button, istouch)
    if self.callbacks.mousepressed then
        self.callbacks.mousepressed(self, x, y, button, istouch)
    end
end

function MSGBOX:update(delta)
    if self.callbacks.update then
        self.callbacks.update(self, delta)
    end
end

function MSGBOX:parseInputs(key, scancode, isrepeat)
    if self.callbacks.onKeyPress then
        self.callbacks.onKeyPress(self, key, scancode, isrepeat)
    end
end

local message = {}

message.MSGBOXStack = stack:new()

--[[
--  Takes coroutine for update, 
--  functions for rendering/input
--]]

function message.pushMSG(title, width, height, callbacks)
    message.MSGBOXStack:push(MSGBOX:new(title, width, height, callbacks))
end

function message.pop()
    message.MSGBOXStack:pop()
end

function message.isShowingMSG()
    return (message.MSGBOXStack.sp>1)
end

function message.render()
    for box in message.MSGBOXStack:list_iter() do
        box:render()
    end
end

function message.update(delta)
    local top = message.MSGBOXStack:peek()
    top:update(delta)
end

function message.mousepressed(x, y, button, istouch)
    local top = message.MSGBOXStack:peek()
    top:mousepressed(x, y, button, istouch)
end

function message.parseInputs(key, scancode, isrepeat)
    local top = message.MSGBOXStack:peek()
    top:parseInputs(key, scancode, isrepeat)
end

return message
