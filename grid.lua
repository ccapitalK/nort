local M = {state = {}, width = 600, height = 600}

function M.initGrid()
    for i = 1, M.width-1 do
        M.state[i] = {}
        for j = 1, M.height-1 do
            M.state[i][j]=false
        end
    end
end 

function M.set(x,y,t)
    M.state[x][y]=t
end

function M.get(x,y)
    return M.state[x][y]
end

function M.toggle(x,y)
    M.state[x][y]=(M.state[x][y]==false)
end

return M
