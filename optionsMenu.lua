--[[
--  Module for the options menu
--]]
buttons = require('buttons')
mainMenu = require('mainMenu')
colors = require('colors')
options = require('options')
fonts = require('fonts')
state = require('state')
local optionsMenu = {}

optionsMenu.currentPage = 1
local maxPage = 3

function optionsMenu.nextPage()
    optionsMenu.currentPage = optionsMenu.currentPage + 1
    if optionsMenu.currentPage > maxPage then
        optionsMenu.currentPage = 1
    end
end

local backButton        = buttons.Button:new( 75,450,175,100,"BACK",(function()state.transition('mainMenu')end) )
local nextPageButton    = buttons.Button:new(275,450,250,100,"NEXT >",(function()optionsMenu.nextPage()end) )
optionsMenu.is   = {}
optionsMenu.cs   = {}
for i = 1, 4 do
    optionsMenu.is[i]= {}
    optionsMenu.cs[i]= {}
end

optionsMenu.cs.background = {}
optionsMenu.cs.foreground = {}

local playerCountButton = {}

function optionsMenu.load()
    state.preHook["optionsMenu"]=function()
        optionsMenu.currentPage=1
    end
    optionsMenu.currentPage=1
    for i = 1, 4 do
        optionsMenu.is[i] = buttons.InputSelection:new(i,225,172+(50*i),300,40)
        optionsMenu.cs[i]  = buttons.colorSelector:new(
            225, 172+(i*50), 300, 40,
            function()
                return options.player[i].Color
            end,
            function(colorValue)
                options.player[i].Color=colorValue
            end
        )
    end
    optionsMenu.cs.background  = buttons.colorSelector:new(
        225, 272, 300, 40,
        function()
            return colors.background
        end,
        function(colorValue)
            colors.background=colorValue
        end
    )
    optionsMenu.cs.foreground  = buttons.colorSelector:new(
        225, 322, 300, 40,
        function()
            return colors.foreground
        end,
        function(colorValue)
            colors.foreground=colorValue
        end
    )
    playerCountButton = buttons.Button:new(400,222,125,40,
        options.numPlayers.."/"..options.maxPlayers,
        function(this)
            options.toggleNumPlayers()
            this.text = options.numPlayers.."/"..options.maxPlayers
        end,
        fonts.optionsFont)
end

function optionsMenu.activateButtons()
    for i = 1, 4 do
        optionsMenu.is[i]:activate()
    end
    backButton:activate()
    nextPageButton:activate()
end

function optionsMenu.deactivateButtons()
    for i = 1, 4 do
        optionsMenu.is[i]:deactivate()
    end
    backButton:deactivate()
    nextPageButton:deactivate()
end


function optionsMenu.render()
    love.graphics.setColor(colors.foreground)
    love.graphics.setFont(fonts.titleFont)
    love.graphics.printf("OPTIONS", 50, 25, 500, "center")
    love.graphics.setFont(fonts.optionsFont)
    local pageString = "PAGE " .. optionsMenu.currentPage .. "/" .. maxPage .. ":"
    love.graphics.printf(pageString, 80, 175, 140, "left")
    backButton:render()
    nextPageButton:render()
    if optionsMenu.currentPage==1 then
        love.graphics.setFont(fonts.optionsFont)
        love.graphics.printf( "LEFT",  225, 175, 140, "center")
        love.graphics.printf("RIGHT",  385, 175, 140, "center")
        for i = 1, 4 do
            love.graphics.setFont(fonts.optionsFont)
            love.graphics.printf("PLAYER "..i..":",  80, 175+(50*i), 140, "left")
            optionsMenu.is[i]:render()
        end
    elseif optionsMenu.currentPage==2 then
        love.graphics.setFont(fonts.optionsFont)
        love.graphics.printf("COLOR",  225, 175, 300, "center")
        for i = 1, 4 do
            love.graphics.setColor(colors.foreground)
            love.graphics.setFont(fonts.optionsFont)
            love.graphics.printf("PLAYER "..i..":",  80, 175+(50*i), 140, "left")
            optionsMenu.cs[i]:render()
        end
        love.graphics.setColor(colors.foreground)
    elseif optionsMenu.currentPage==3 then
        love.graphics.setFont(fonts.optionsFont)
        love.graphics.printf("NUMBER OF PLAYERS:",  80, 225, 300, "left")
        playerCountButton:render()
        love.graphics.setColor(colors.foreground)
        love.graphics.printf("Background",  80, 275, 140, "left")
        optionsMenu.cs.background:render()
        love.graphics.setColor(colors.foreground)
        love.graphics.printf("Foreground",  80, 325, 140, "left")
        optionsMenu.cs.foreground:render()
    end
end

function optionsMenu.update()

end

function optionsMenu.mousepressed(x, y, button, istouch)
    if button == 1 then
        backButton:handleMousePress(x, y)
        nextPageButton:handleMousePress(x, y)
        if optionsMenu.currentPage == 1 then
            for i = 1, 4 do
                optionsMenu.is[i]:handleMousePress(x, y)
            end
        elseif optionsMenu.currentPage == 2 then 
            for i = 1, 4 do
                optionsMenu.cs[i]:handleMousePress(x, y)
            end
        elseif optionsMenu.currentPage == 3 then 
            optionsMenu.cs.background:handleMousePress(x, y)
            optionsMenu.cs.foreground:handleMousePress(x, y)
            playerCountButton:handleMousePress(x, y, playerCountButton)
        end
    end
end

return optionsMenu
