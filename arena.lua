--[[
--  Handle the main game scenario
--]]
require('utility')
grid   = require('grid')
players= require('player')
colors = require('colors')
options= require('options')
state  = require('state')
message= require('message')

local arena = {}

function arena.load()
    state.preHook["arena"] = arena.setup
end

function arena.setup()
    players.setupPlayers(options.numPlayers)
    players.numAlive=options.numPlayers
    grid.initGrid()
    players.initCanvas()
end

function arena.draw()
    love.graphics.setBlendMode("alpha", "premultiplied")
    love.graphics.draw(players.canvas)
    love.graphics.setBlendMode("alpha")
    for i = 1, options.numPlayers do
        players[i]:draw()
    end
end

function arena.parseInputs(key, scancode, isrepeat)
    if isrepeat == false then
        if key == "escape" then
            state.transition('mainMenu')
        end
        for i = 1, options.numPlayers do
            if key == players[i].leftKey then
                players[i]:turnLeft()
            elseif key == players[i].rightKey then
                players[i]:turnRight()
            end
        end
    end
end

function arena.update(delta)
    for i = 1, options.numPlayers do
        if players[i].alive then
            players[i]:move(delta)
        end
    end
    if players.numAlive <= 1 then
        local alivePlayer = 1
        for i = 1, options.numPlayers do
            if players[i].alive then
                alivePlayer = i
                break
            end
        end
        message.pushMSG(
            "Player " .. alivePlayer .. " wins! Resetting...",
            300,
            200,
            {
                setup=function(self)
                    self.var = {
                        timer = 0
                    }
                end,
                update=function(self, delta)
                    self.var.timer = self.var.timer + delta
                    if self.var.timer > 1 then
                        arena.setup()
                        message.pop()
                    end
                end,
                onKeyPress=function(self, key, scancode, isrepeat)
                    if key == "escape" then
                        state.transition('mainMenu')
                        message.pop()
                    end
                end
            }
        )
    end
end

return arena
