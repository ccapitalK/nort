--[[
--  Module containg option values 
--]]

colors = require('colors')
local options = {}

options.player = {}
options.player[1] = {}
options.player[2] = {}
options.player[3] = {}
options.player[4] = {}
options.numPlayers= {}
options.maxPlayers= {}

function options.toggleNumPlayers()
    options.numPlayers=options.numPlayers+1
    if options.numPlayers > options.maxPlayers then
        options.numPlayers=2
    end
end

function options.load()
    options.player[1].LKey='a'
    options.player[1].RKey='d'
    options.player[1].Color=colors.white
    options.player[2].LKey='left'
    options.player[2].RKey='right'
    options.player[2].Color=colors.white
    options.player[3].LKey='v'
    options.player[3].RKey='n'
    options.player[3].Color=colors.white
    options.player[4].LKey='i'
    options.player[4].RKey='p'
    options.player[4].Color=colors.white
    options.numPlayers=2
    options.maxPlayers=4
end

return options
