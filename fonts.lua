--[[
--  Module containing global table
--]]

local fonts = {}

fonts.titleFont   = love.graphics.newFont("fonts/CPMono_v07_Bold.otf", 96)
fonts.mainFont    = love.graphics.newFont("fonts/CPMono_v07_Bold.otf", 48)
fonts.optionsFont = love.graphics.newFont("fonts/CPMono_v07_Bold.otf", 20)

return fonts
