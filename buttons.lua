--[[
--  Module containing the button class
--]]

colors = require('colors')
options = require('options')
fonts = require('fonts')
local buttons = {}

buttons.Button = {}

function buttons.Button:new(_x, _y, _w, _h, _text, _onPress, _font)
    newObj = {
        x  = _x,
        y  = _y,
        w = _w,
        h = _h,
        font = _font or fonts.mainFont,
        isActive = true,
        onPress = _onPress,
        text = _text,
        wasHover = false
    }
    self.__index = self
    return setmetatable(newObj, self)
end

function buttons.Button:isHover()
    if self.isActive then
        self.wasHover = ((inRange(self.x, love.mouse.getX(), self.x+self.w) and
                inRange(self.y, love.mouse.getY(), self.y+self.h)))
    end
    return self.wasHover
end

function buttons.Button:activate()
    self.isActive = true
end

function buttons.Button:deactivate()
    self.isActive = false
end

function buttons.Button:render()
    local vOffset = (self.h-(self.font:getHeight()*(75/67)))/2
    love.graphics.setColor(colors.foreground)
    if self:isHover() then
        --The mouse is hovering over this button
        love.graphics.rectangle("fill", self.x, self.y, self.w, self.h)
        love.graphics.setColor(colors.background)
        love.graphics.setFont(self.font)
        love.graphics.printf(self.text, self.x, self.y+vOffset, self.w, "center")
        love.graphics.setColor(colors.foreground)
    else
        --The mouse is not over this button
        love.graphics.setLineWidth(2)
        love.graphics.rectangle("line", self.x, self.y, self.w, self.h)
        love.graphics.setFont(self.font)
        love.graphics.printf(self.text, self.x, self.y+vOffset, self.w, "center")
    end
end

function buttons.Button:handleMousePress(x, y, userData)
    if self:isHover() and self.isActive then
        self:onPress(userData)
    end
end

buttons.InputSelection = {}

function buttons.InputSelection:new(playerNum, x, y, w, h)
    local newObj = {
        playerOptions = options.player[playerNum],
        leftButton = buttons.Button:new(
            x, y, w/2-10, h, options.player[playerNum].LKey,
            function (userData)
                optionsMenu.deactivateButtons()
                message.pushMSG(
                    "Please press button for Player "..playerNum.." Turn Left, escape to cancel",
                    300, 
                    200,
                    {
                        onKeyPress=function(self, key, scancode, isrepeat)
                            if key ~= "escape" then
                                options.player[playerNum].LKey=key
                                userData.text=key
                            end
                            optionsMenu.activateButtons()
                            message.pop()
                        end
                    }
                )
                --self.text=self.playerOptions.LKey
            end,
            fonts.optionsFont
        ),
        rightButton = buttons.Button:new(
            x+w/2+10, y, w/2-10, h, options.player[playerNum].RKey,
            function (userData)
                optionsMenu.deactivateButtons()
                message.pushMSG(
                    "Please press button for Player "..playerNum.." Turn Right, escape to cancel",
                    300, 
                    200,
                    {
                        onKeyPress=function(self, key, scancode, isrepeat)
                            if key ~= "escape" then
                                options.player[playerNum].RKey=key
                                userData.text=key
                            end
                            optionsMenu.activateButtons()
                            message.pop()
                        end
                    }
                )
                --self.text=self.playerOptions.RKey
            end,
            fonts.optionsFont
        )
    }
    self.__index = self
    return setmetatable(newObj, self)
end

function buttons.InputSelection:render()
    self.leftButton:render()
    self.rightButton:render()
end

function buttons.InputSelection:activate()
    self.leftButton:activate()
    self.rightButton:activate()
end

function buttons.InputSelection:deactivate()
    self.leftButton:deactivate()
    self.rightButton:deactivate()
end

function buttons.InputSelection:handleMousePress(x, y)
    self. leftButton:handleMousePress(x, y, self)
    self.rightButton:handleMousePress(x, y, self)
end

buttons.colorSelector = {}

function buttons.colorSelector:new(x, y, w, h, currentColorFunc, onChoose)
    if onChoose == nil then
        error("color selector constructed without onChoose callback")
    end
    if w ~= 300 or h ~= 40 then
        error("color selector can only be 300x40")
    end
    local newObj = {
        x = x,
        y = y,
        w = w,
        h = h,
        currentColorFunc = currentColorFunc,
        onChoose = onChoose
    }
    self.__index = self
    return setmetatable(newObj, self)
end

--[[
--  onPress(colorValue)
--]]

function buttons.colorSelector:render()
    local colorToCompare = self.currentColorFunc()
    for i = 0, 9 do
        love.graphics.setColor(colors.foreground)
        love.graphics.rectangle("fill", self.x+5+(30*i), self.y+10, 20, 20)
        love.graphics.setColor(colors.arr[i+1])
        love.graphics.rectangle("fill", self.x+7+(30*i), self.y+12, 16, 16)
        if colorToCompare == colors.arr[i+1] then
            love.graphics.setColor(colors.foreground)
            love.graphics.polygon(
                "fill", 
                self.x+8+(30*i), 
                self.y+44, 
                self.x+16+(30*i), 
                self.y+36, 
                self.x+23+(30*i), 
                self.y+44 
            )
            love.graphics.rectangle("fill", self.x+7+(30*i), self.y+44, 16, 4)
        end
    end
end

function buttons.colorSelector:handleMousePress(x, y)
    for i = 0, 9 do
        local x1 = self.x+5+(30*i)
        local y1 = self.y+10
        local x2 = x1+20
        local y2 = y1+20
        if inRange(x1, x, x2) and inRange(y1, y, y2) then
            self.onChoose(colors.arr[i+1])
        end
    end
end

return buttons
