--[[
--  Misc functions and data structures
--]]

--[[
--  Stack data structure used by message handler
--]]

stack = {}

function stack:new()
    newObj = {
        sp = 1,
        data = {}
    }
    self.__index = self
    return setmetatable(newObj, self)
end

function stack:push(value) 
    self.data[self.sp] = value
    self.sp = self.sp + 1
end

function stack:peek()
    return self.data[self.sp-1]
end

function stack:pop()
    if self.sp > 1 then
        self.sp = self.sp - 1
    end
    if self.sp > 1 then
        return self.peek()
    end
    return
end

function stack:list_iter()
    local i = 0
    local n = self.sp
    return function()
            i = i + 1
            if i <= n then
                return self.data[i]
            else
                return
            end
        end
end

function math.round(n, deci) 
    local deci = 10^(deci or 0) 
    return math.floor(n*deci+.5)/deci 
end

function math.clamp(low, n, high) 
    return math.min(math.max(n, low), high) 
end

function countWrapLines(text, font, boxW)
    local spaceWidth = font:getWidth(" ")
    local nLines = 1
    local cWidth = 0
    for word in text:gmatch("%S+") do
        local wordWidth = font:getWidth(word)
        if cWidth + wordWidth > boxW then
            cWidth = 0
            nLines = nLines + 1
        end
        cWidth = cWidth + wordWidth + spaceWidth
    end
    return nLines
end

function error(message)
    print("Error: "..message)
    message[undefined]=2
end

function inRange(min,val,max)
    return (min <= val and val <= max)
end
