--[[
--  Module for the main menu
--]]

require('utility')
buttons = require('buttons')
fonts = require('fonts')
state = require('state')

local mainMenu = {}

local playButton    = buttons.Button:new(175,200,250,100,"PLAY",(function()state.transition('arena')end) )
local optionsButton = buttons.Button:new(175,325,250,100,"OPTIONS",(function()state.transition('optionsMenu')end) )
local quitButton    = buttons.Button:new(175,450,250,100,"QUIT",(function()love.event.quit()end) )

function mainMenu.load()
end

function mainMenu.render()
    love.graphics.setColor(colors.foreground)
    love.graphics.setFont(fonts.titleFont)
    love.graphics.printf("NORT", 100, 25, 400, "center")
    playButton:render()
    optionsButton:render()
    quitButton:render()
end

function mainMenu.update()
    
end

function mainMenu.mousepressed(x, y, button, istouch)
    if button == 1 then
        playButton:handleMousePress(x, y)
        optionsButton:handleMousePress(x, y)
        quitButton:handleMousePress(x, y)
    end
end

return mainMenu
