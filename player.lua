require('utility')
grid=require('grid')
colors=require('colors')
options=require('options')
local M = {}

Player = {}

function Player:new()
    newObj = {
        x=0,
        y=0,
        px = 0,
        py = 0,
        dir=0,
        leftKey="",
        rightKey="",
        alive=false
    }
    self.__index = self
    return setmetatable(newObj, self)
end

function Player:setValues(_x,_y,_dir,_lK,_rK,col)
    self.x = _x
    self.y = _y
    self.px = self.x
    self.py = self.y
    self.dir = _dir
    self.leftKey  = _lK
    self.rightKey = _rK
    self.alive=true
    self.color=col or colors.foreground
end

M = {
        Player:new(), 
        Player:new(),
        Player:new(),
        Player:new() 
    }

function M.setupPlayers(numPlayers)
    M[1]:setValues(30,300,2,     options.player[1].LKey,options.player[1].RKey,options.player[1].Color)
    M[2]:setValues(570,299,4,    options.player[2].LKey,options.player[2].RKey,options.player[2].Color)
    if numPlayers >= 3 then
        M[3]:setValues(299,30,3, options.player[3].LKey,options.player[3].RKey,options.player[3].Color)
    end
    if numPlayers >= 4 then
        M[4]:setValues(300,570,1,options.player[4].LKey,options.player[4].RKey,options.player[4].Color)
    end
end

M.playerVelocity=2.5
M.canvas=love.graphics.newCanvas(600, 600)

--[[
--  Direction to int mapping
--  up    = 1
--  right = 2
--  down  = 3
--  left  = 4
--]]

function Player:getPos()
    return self.x, self.y, self.dir
end

function Player:draw()
    --print(option
    local cx, cy = self:getPos()
    love.graphics.setLineWidth(1)
    love.graphics.setColor(self.color)
    love.graphics.line(self.px, self.py, self.x, self.y)
    love.graphics.setColor(colors.foreground)
    if self.alive then
        love.graphics.rectangle("fill", cx-1, cy-1, 3, 3)
    end
end

function Player:die()
    self.alive = false
    M.numAlive = M.numAlive - 1
end

function Player:move(delta)
    local cx, cy, cdir = self:getPos()
    local ex, ey = cx, cy
    local step = 0
    local movDist=delta*60.0*M.playerVelocity
    if cdir % 2 == 0 then
        --moving on x-axis
        if (cdir / 2) <= 1 then
            --moving right
            ex=cx+movDist
            step=1
        else
            --moving left
            ex=cx-movDist
            step=-1
        end
    else
        --moving on y-axis
        if (cdir / 2) < 1 then
            --moving up
            ey=cy-movDist
            step=-1
        else
            --moving down
            ey=cy+movDist
            step=1
        end
    end
    local rcx, rcy, rex, rey = math.round(cx), math.round(cy), math.round(ex), math.round(ey)
    if (rey >= grid.height) or (rey <= 0) then
        self:die()
        self.x, self.y = ex, math.clamp(0,ey,grid.height)
    elseif (rex >= grid.width) or (rex <= 0) then
        self:die()
        self.x, self.y = math.clamp(0,ex,grid.width), ey
    else
        if cx == ex then
            local x = rcx
            for y = rcy, rey-step, step do
                if grid.get(x,y) == false then
                    grid.set(x,y,true)
                else
                    self:die()
                    self.x, self.y = x, y
                    return
                end
            end
        else
            local y = rcy
            for x = rcx, rex-step, step do
                if grid.get(x,y) == false then
                    grid.set(x,y,true)
                else
                    self:die()
                    self.x, self.y = x, y
                    return
                end
            end
        end
    end
    if self.alive then
        self.x, self.y = ex, ey
    end
end

function Player:updateCanvas()
    love.graphics.setCanvas(M.canvas)
        love.graphics.setLineWidth(1)
        love.graphics.setColor(self.color)
        love.graphics.line(self.px, self.py, self.x, self.y)
        love.graphics.setColor(colors.foreground)
    love.graphics.setCanvas()
end

function Player:turnLeft()
    self.dir=({4,1,2,3})[self.dir]
    self:updateCanvas()
    self.px, self.py = self.x, self.y
end

function Player:turnRight()
    self.dir=({2,3,4,1})[self.dir]
    self:updateCanvas()
    self.px, self.py = self.x, self.y
end

function M.initCanvas()
    love.graphics.setCanvas(M.canvas)
        love.graphics.clear(0, 0, 0, 0)
    love.graphics.setCanvas()
    --love.graphics.setBlendMode("alpha")
end

return M
